package hr.apps.mosaic.example_98;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

	private static final String NAME = "name";
	private static final String SURNAME = "surname";
	private static final String EMPTY_STRING = "";


	@BindView(R.id.etName) EditText etName;
	@BindView(R.id.etSurname) EditText etSurame;
	@BindView(R.id.bSaveUserDetails) Button bSaveUserDetails;
	@BindView(R.id.bDisplayUserDetails) Button bDisplayUserDetails;

	String mName = "", mSurname = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);

		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey(NAME)) {
				this.mName = savedInstanceState.getString(NAME);
			}
			if (savedInstanceState.containsKey(SURNAME)) {
				this.mSurname = savedInstanceState.getString(SURNAME);
			}
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// Uobičajeno se ključevi definiraju kao konstante
		outState.putString(NAME, this.mName);
		outState.putString(SURNAME, this.mSurname);
		super.onSaveInstanceState(outState);
	}

	@OnClick(R.id.bDisplayUserDetails)
	public void diplayUserDetails(){
		String message = "Hello " + this.mName + " " + this.mSurname;
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	@OnClick(R.id.bSaveUserDetails)
	public void saveUserDetails(){
		this.mName = this.etName.getText().toString();
		this.mSurname = this.etSurame.getText().toString();
		this.etName.setText(EMPTY_STRING);
		this.etSurame.setText(EMPTY_STRING);
	}
}